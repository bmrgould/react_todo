import React, { Component } from 'react';
import { BrowserRouter as Router, Route } from 'react-router-dom';
import { Link } from 'react-router-dom';
import CHeader from './components/Layout/CHeader';
import Todos from './components/Todos';
import AddTodo from './components/AddTodo';
import About from './components/pages/About';
//import {v4 as uuid} from 'uuid';
import axios from 'axios';
import { Layout, Menu, Breadcrumb } from 'antd';

import './App.css';

const { Header, Content, Footer } = Layout;

class App extends Component {
  state = {
    todos: []
  }

  componentDidMount() {
    axios.get('https://jsonplaceholder.typicode.com/todos?_limit=10')
      .then(res => this.setState({ todos: res.data }));
  }

  // Toogles Complete
  toggleComplete = (id) => {
    this.setState({ todos: this.state.todos.map(todo => {
      if (todo.id === id) {
        todo.completed = !todo.completed
      }
      return todo;
    }) });
  }

  // Deletes a Todo
  delTodo = (id) => {
    axios.delete(`https://jsonplaceholder.typicode.com/todos/${id}`)
      .then(res => this.setState({ todos: [...this.state.todos.filter(todo => todo.id !== id )] }));
  }

  // Add Todo
  addTodo = (title) => {
    axios.post('https://jsonplaceholder.typicode.com/todos', {
      title: title,
      completed: false
    })
    .then(res => this.setState({ todos: [...this.state.todos, res.data] }));
    
  }

  render() {
      return (
        <Router>
          <Layout className="layout">

            <Header>
              <Menu theme="dark" mode="horizontal" defaultSelectedKeys={['1']}>
                <Menu.Item key="1"><Link to="/">Todo List</Link></Menu.Item>
                <Menu.Item key="2"><Link to="About">About</Link></Menu.Item>
              </Menu>
            </Header>

            <Content style={{ padding: '0 50px' }}>
              <Route exact path="/" render={props => (
                <React.Fragment>
                  <AddTodo addTodo={this.addTodo} />
                  <Todos  todos={this.state.todos} toggleComplete={this.toggleComplete} delTodo={this.delTodo} />
                </React.Fragment>
              )} />
              <Route path="/about" component={About}/>
            </Content>

            <Footer style={{ textAlign: 'center' }}>BMRGould ©2020</Footer>

          </Layout>
        </Router>
      )
  };
}

export default App;
