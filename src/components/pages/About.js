import React from 'react'

export default function About() {
    return (
        <React.Fragment>
            <h1>About</h1>
            <p>Current: Version 1.1.0.</p>
            <p>Version 1.1.0: Ant Design added as the design system choice.</p>
            <p>Version 1.0.0: Created by following a React Crash Course.</p>
        </React.Fragment>
    )
}
