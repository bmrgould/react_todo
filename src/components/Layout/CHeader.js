import React from 'react';
import { Link } from 'react-router-dom';
import { Button } from 'antd';
import 'antd/dist/antd.css';

export default function CHeader() {
    return (
        <header style={headerStyle}>
            <h1>Todo List</h1>
            <Link style={linkStyle} to="/">Home</Link> | <Link style={linkStyle} to="About">About</Link>
        </header>
    )
}


const headerStyle = {
    background: '#333',
    color: '#fff',
    textAlign: 'center',
    padding: '10px'
}

const linkStyle = {
    color: '#fff',
    textDecoration: 'none'
}