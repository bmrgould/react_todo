import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Button, Checkbox } from 'antd';
import 'antd/dist/antd.css';

export class TodoItem extends Component {

    getStyle = () => {
        return {
            background: '#f4f4f4',
            padding: '10px',
            borderBottom: '1px #ccc dotted',
            textDecoration:  this.props.todo.completed ?
            'line-through' : 'none'
        }
    }



    render() {
        const { id, title, completed } = this.props.todo; //deconstruct to pull out values

        return (
            <div style={this.getStyle()}>
                <p>
                    <Checkbox checked={completed} onChange={ this.props.toggleComplete.bind(this, id) } > </Checkbox>
                    {" "}
                    { title }
                    <Button type="dashed" danger onClick={this.props.delTodo.bind(this, id)} style={btnStyle}>x</Button>
                </p>
            </div>
        )
    }
}

// PropTypes
TodoItem.propTypes = {
    todo: PropTypes.object.isRequired,
    toggleComplete: PropTypes.func.isRequired,
    delTodo: PropTypes.func.isRequired
}

const btnStyle = {
    cursor: 'pointer',
    float: 'right'
}


export default TodoItem
